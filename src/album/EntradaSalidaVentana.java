package album;
import javax.swing.JOptionPane;

public class EntradaSalidaVentana {
 
    public static int leerDatoEntero(String mensaje)
    {
        return Integer.parseInt(JOptionPane.showInputDialog(mensaje));
    }
    public static float leerDatoReal(String mensaje)
    {
        return Float.parseFloat(JOptionPane.showInputDialog(mensaje));
    }
    public static void mostrarCadena(String mensaje)
    {
        JOptionPane.showMessageDialog(null, mensaje);
    }
    public static String leerCadena(String mensaje){
        return JOptionPane.showInputDialog(mensaje);
    }
    public static char leerCaracter(String mensaje){
        return leerCadena(mensaje).charAt(0);
    }
    
}
