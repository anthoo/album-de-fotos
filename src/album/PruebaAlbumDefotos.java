package album;
/**
 *
 * @author Anthoo
 */
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class PruebaAlbumDefotos {
       static Album miAlbum = new Album("habitaDev", 3);

    public static void main(String[] args) {
        // TODO code application logic here
        leerFotos();
        System.out.println("\nFOTOS:");
        miAlbum.mostrarFotos();
        int umbral;        
        umbral = EntradaSalidaVentana.leerDatoEntero("Ingrese umbral para detectar fotos defectuosas: ");
        miAlbum.mostrarFotosUmbral(umbral);       
        
    }

    public static void leerFotos() {
        String personasEnFoto1[] = {"Anita", "Nahuel", "Maru", "Antonia", "judith"};
        String nombreArchivo;
        int tamaño;
        String descripcion;
        //Variable auxiliar que contendrá la referencia a cada foto nueva.
        Foto aux = null;
        int i, N;
        //se pide por teclado el número de fotos a leer
        do {
            N = EntradaSalidaVentana.leerDatoEntero("Cantidad de foto a ingresar? ");
        } while (N < 0);
        for (i = 1; i <= N; i++) {
            //leer datos de cada foto
            EntradaSalidaVentana.mostrarCadena("foto " + i);
            EntradaSalidaVentana.mostrarCadena("Se debiera ingresar cada una de las personas que se encuentran en una foto\n pero Siempre seran las mismas sus nombres esta en un array");
            nombreArchivo = EntradaSalidaVentana.leerCadena("Nombre de archivo: ");
            tamaño = EntradaSalidaVentana.leerDatoEntero("Tamaño: ");
            descripcion = EntradaSalidaVentana.leerCadena("descripcion: ");
            aux = new Foto(nombreArchivo); //Se crea un objeto Foto y se asigna su referencia a aux
            //se asignan valores a los atributos del nuevo objeto
            aux.setPersonaEnFoto(personasEnFoto1);
            aux.setTamaño(tamaño);
            aux.descripcion = descripcion;
            //se añade el objeto al final del array addFoto(Foto foto) 
            miAlbum.addFoto(aux);
        }
    } //fin método LeerFotos()

}
