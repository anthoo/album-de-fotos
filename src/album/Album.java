/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package album;

import java.util.ArrayList;
import java.util.Date;

public class Album {

    private Date fechaCreacion = new Date();
    private ArrayList<Foto> fotos;
    private String nombre;
    private int cantidadMaxima;

    public Album(String nombre, int cantidadMaxima) {
        this.nombre = nombre;
        this.cantidadMaxima = cantidadMaxima;
        this.fotos = new ArrayList<Foto>();
    }

    // 9.B Agrega una foto nueva a la lista de fotos
    public void addFoto(Foto foto) {
        fotos.add(foto);
    }

    //Obtiene una foto de la lista de fotos dada una posicion en dicha lista
    public Foto getFoto(int posicion) {
        Foto unaFoto = fotos.get(posicion);
        return unaFoto;
    }

    public int getCantidadMaxima() {
        return cantidadMaxima;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public String getNombre() {
        return nombre;
    }

    //Ejercicio 2 Metodo que permite saber si en el album hay fotos con tamaño menor a cierto umbral
    public boolean tieneFotoConTamañoMenor(int umbral) {
        boolean fotoDañada = true;
        for (int i = 0; i <= this.fotos.size(); i++) {
            if (this.fotos.get(i).tamaño > umbral) {
                fotoDañada = false;
            }
        }
        return fotoDañada;
    }

    public ArrayList<Foto> nuevaFuncionalidad(int umbral) {
        ArrayList<Foto> r = new ArrayList<Foto>();
        for (Foto fotoActual : this.fotos) {
            if (fotoActual.getTamaño() <= umbral) {
                int posInsert = 0;
                boolean encontrado = false;
                while (posInsert < r.size() && !encontrado) {
                    Foto fotoListaRetorno = r.get(posInsert);
                    if (fotoActual.getTamaño() > fotoListaRetorno.getTamaño()) {
                        encontrado = true;
                    } else {
                        posInsert = posInsert + 1;
                    }
                }
                r.add(posInsert, fotoActual);
            }
        }
        return r;
    }

    public void mostrarFotos() {
        for (int i = 0; i < fotos.size(); i++) {
            EntradaSalidaVentana.mostrarCadena(fotos.get(i).toString());
        }   
    }
 

    public void mostrarFotosUmbral(int u) {
        ArrayList<Foto> r = nuevaFuncionalidad(u);
        for (int i = 0; i < r.size(); i++) {
            EntradaSalidaVentana.mostrarCadena(r.get(i).toString());
        }
    }
}
