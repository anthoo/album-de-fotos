/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package album;

public class Foto {

    String personasEnFoto[];
    String nombreArchivo;
    int tamaño;
    String descripcion;

    public Foto(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getPersonaEnFoto(int pos) {
        String alguien = personasEnFoto[pos];
        return alguien;
    }
//completar metodo

    public void setPersonaEnFoto(int pos, String persona) {
        this.personasEnFoto = personasEnFoto;
    }

    public void setPersonaEnFoto(String personas[]) {
        this.personasEnFoto = personas;
    }

    public int getTamaño() {
        return tamaño;
    }

//agregue este metodo
    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    public String MostrarPersonasEnFoto() {
        String personas = "\t";
        for (int i = 0; i < personasEnFoto.length; i++) {           
         personas= personas +(personasEnFoto[i]) + ", ";
        }
        return personas;
//         EntradaSalidaVentana.mostrarCadena( personas);
       
    }

    @Override
    public String toString() {
        return "Personas en foto: \t" + MostrarPersonasEnFoto() + "\n Nombre del archivo: \t" + nombreArchivo + "\n Tamaño: \t" + tamaño + "\n Descripcion: \t" + descripcion;
    }

    public void addPersonaEnFotos(int i, String nom) {
        //fotos.add(foto);
        personasEnFoto[i] = nom;
    }

}
